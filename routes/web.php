<?php


use App\Http\Controllers\Admin\ProductPropertyController;
use App\Http\Controllers\Admin\PropertyController;
use App\Http\Controllers\Admin\PropertyGroupController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\CategoriController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Client\CommentController;
use App\Http\Controllers\Admin\CommentController as AdminCommentController;
use App\Http\Controllers\Client\ProductController as ClientProductController;
use App\Http\Controllers\client\HomeController;
use App\Http\Controllers\Admin\PictureController;
use App\Http\Controllers\Admin\DiscountController;
use App\Http\Controllers\Client\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->name('client.')->group(function (){
    Route::get('/', [HomeController::class , 'index'])->name('index');

    Route::get('products/{product}',[ClientProductController::class , 'show'])->name('products.show');

   Route::get('/register',[RegisterController::class, 'create'])->name('register');
   Route::post('/register/sendmail',[RegisterController::class, 'sendmail'])->name('register.sendmail');
   Route::get('/register/otp/{user}',[RegisterController::class, 'otp'])->name('register.otp');
   Route::post('/register/verifyOtp/{user}',[RegisterController::class, 'verifyOtp'])->name('register.verifyOtp');
   Route::delete('/register/logout',[RegisterController::class, 'logout'])->name('register.logout');

   Route::post('products/comment/{product}/store',[CommentController::class,'store'])->name('product.comment.store');


});





Route::prefix('/adminpanel')/*->middleware([
    \App\Http\Middleware\CheckPermission::class .':readCat',
    'auth'

]) */    ->group(function (){
Route::get('/', function (){
    return view('admin.home');
});

    Route::resource('categories',CategoriController::class);
/*    Route::get('/categories',[App\Http\Controllers\Admin\CategoriController::class,'index'])->name('categories.index');
    Route::get('/categories/create',[App\Http\Controllers\Admin\CategoriController::class,'create'])->name('categories.create');
    Route::post('/categories/store',[App\Http\Controllers\Admin\CategoriController::class,'store'])->name('categories.store');
    Route::get('/categories/{categori}',[App\Http\Controllers\Admin\CategoriController::class,'edit'])->name('categories.edit');
    Route::patch('/categories/{categori}/edit',[App\Http\Controllers\Admin\CategoriController::class,'update'])->name('categories.update');
    Route::delete('/categories/{categori}',[App\Http\Controllers\Admin\CategoriController::class,'destroy'])->name('categories.destroy');*/
Route::resource('brands', \App\Http\Controllers\admin\BrandController::class);

Route::resource('products',ProductController::class);

Route::resource('products.pictures',PictureController::class);

Route::resource('products.discounts',DiscountController::class);

Route::resource('roles',RoleController::class);

Route::resource('users',UserController::class);

Route::resource('propertyGroup',PropertyGroupController::class);

Route::resource('property',PropertyController::class);

Route::get('products/{product}/propertyProduct/create',[ProductPropertyController::class,'create'])->name('products.propertyProduct.create');
Route::get('products/{product}/propertyProduct',[ProductPropertyController::class,'index'])->name('products.propertyProduct.index');
Route::post('products/{product}/propertyProduct',[ProductPropertyController::class,'store'])->name('products.propertyProduct.store');
    Route::get('products/{product}/comment/index',[AdminCommentController::class,'index'])->name('products.comment.index');
    Route::delete('products/{comment}/comment/destroy',[AdminCommentController::class,'destroy'])->name('products.comment.destroy');
});



