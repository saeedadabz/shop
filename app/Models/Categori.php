<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categori extends Model
{
    use HasFactory;
    protected $guarded = [] ;

    public function parent()
    {
        return $this->belongsTo(Categori::class, 'categori_id');
    }

    public function children()
    {
        return $this->hasMany(Categori::class , 'categori_id');
    }

    public function allSubcatProducts()
    {
        $childrenIds=$this->children()->pluck('id');
        return Product::query()->whereIn('categori_id',$childrenIds)
            ->orWhere('categori_id',$this->id)
            ->get();
    }

    public function gethaschildrenattribute()
    {
        return $this->children()->count()>0;
    }

    public function Propgroups()
    {
        return $this->belongsToMany(PropertyGroup::class);
    }

}
