<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $guarded=[];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function  hasper($permissionId)
    {
        return $this->permissions()->where('id',$permissionId)->exists();
    }

    public static function findbytitle($title)
    {
        return self::query()->where('title',$title)->firstOrFail();
    }
    //has permission for middleware
    public function haspermission($perTitle)
    {
        return $this->permissions()->where('title',$perTitle)->exists();
    }
}
