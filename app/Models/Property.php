<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function propGroup()
    {
        return $this->belongsTo(PropertyGroup::class,'Property_Group_id');

    }

    public function products()
    {
        return $this->belongsToMany(Product::class)
            ->withPivot(['value'])
            ->withTimestamps();
    }

    public function getvalueforproduct(Product $product)
    {
        $prodpropquery=$this->products()->where('product_id',$product->id);
        if(!$prodpropquery->exists()){
            return null;
        }
        else{
            return $prodpropquery->first()->pivot->value;
        }
    }
}
