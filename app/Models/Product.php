<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function categori()
    {
        return $this->belongsTo(Categori::class, 'categori_id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    public function pictures()
    {
        return $this->hasMany(Picture::class, 'product_id');
    }

    public function discount()
    {
        return $this->hasOne(Discount::class, 'product_id');
    }

    public function addDiscount(\Illuminate\Http\Request $request)
    {
        if (!$this->discount()->exists()) {
            $this->discount()->create([
                'value' => $request->get('value')
                /*'product_id'=> $product->id*/
            ]);
        } else {
            $this->discount->update([
                'value' => $request->get('value')
            ]);
        }
    }

    public function deleteDiscount()
    {
        $this->discount()->delete();
    }

    public function getPriceWithDiscountAttribute()
{
    if (!$this->discount()->exists()){
        return $this->price;
    }
    else{
        return $this->price -$this->price * $this->discount->value /100;
    }
}

    public function properties()
    {
        return $this->belongsToMany(Property::class)
            ->withPivot(['value'])
            ->withTimestamps();
}

    public function comments()
    {
        return $this->hasMany(Comment::class);
}

   /* public function PriceWithDiscount()
    {
        if (!$this->discount()->exists()){
            return $this->price;
        }
        else{
            return $this->price -$this->price * $this->discount->value /100;
        }
    }*/

}
