<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name'=>['required'],
            'image'=>['required','mimes:jpg,jpeg,png,mpeg'],
            'price'=>['required','integer'],
            'slug'=>['required','alpha_dash','unique:products,slug'],
            'description'=>['required','max:100'],
            'categori_id'=>['required','exists:categoris,id'],
            'brand_id'=>['required','exists:brands,id'],
        ];
    }
}
