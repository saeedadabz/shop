<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductPropertyController extends Controller
{
    public function create(Product $product)
    {
        $cat=$product->categori;
        //this propGroups is for cat and cat is for product;
        $propgroups=$cat->Propgroups;
        return view('admin.propertyProduct.create',[
           'product'=>$product,
            'propGroups'=>$propgroups
        ]);
    }

    public function index(Product $product)
    {
        return view('admin.propertyProduct.index',[
           'product'=>$product
        ]);
    }

    public function store(Request $request , Product $product)
    {
        $propertiesval=collect($request->get('properties'))->filter(function ($item) {
            if (!empty($item['value'])) {
                return $item;
            }
        }
        ) ;
       /* dd($propertiesval,$request->get('properties'));*/
        $product->properties()->sync($propertiesval);
        return redirect()->route('products.propertyProduct.index',$product);
    }
}
