<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Brand;
use App\Models\Categori;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
       return view('admin.products.index',[
           'products'=>Product::all()
       ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
       return view('admin.products.create',[
        'categories'=>Categori::all(),
           'brands'=>Brand::all()
       ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateProductRequest $request)
    {
        Product::query()->create([
           'name'=>$request->get('name'),
           'price'=>$request->get('price'),
           'categori_id'=>$request->get('categori_id'),
           'brand_id'=>$request->get('brand_id'),
           'slug'=>$request->get('slug'),
           'image'=>$request->file('image')->storeAs('public/images',$request->file('image')->getClientOriginalName()),
           'description'=>$request->get('description')
        ]);
        return redirect(route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Product $product)
    {
      return view('admin.products.edit',[
         'product'=>$product,
          'categories'=>Categori::all(),
          'brands'=>Brand::all()
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $path=$product->image;
        if ($request->hasFile('image')){
            $path=$request->file('image')->storeAs('public/images',$request->file('image')->getClientOriginalName());
        }

        $product->update([
            'name'=>$request->get('name'),
            'price'=>$request->get('price'),
            'categori_id'=>$request->get('categori_id'),
            'brand_id'=>$request->get('brand_id'),
            'slug'=>$request->get('slug'),
            'image'=>$path,
            'description'=>$request->get('description')
        ]);
        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Product $product)
    {
        $product->pictures()->delete();

       $product->delete();
       return redirect(route('products.index'));
    }
}
