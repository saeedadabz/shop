<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewCatRequest;
use App\Models\Categori;
use App\Models\PropertyGroup;
use Illuminate\Http\Request;

class CategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {

        $categories=Categori::all();
        return view('admin.categories.index',[
        'categories'=>$categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(NewCatRequest $request)
    {
       $categori= Categori::query()->create([
            'title'=>$request->get('title'),
            'categori_id'=>$request->get('categori_id')
        ]);
       $categori->propGroups()->attach($request->get('propGroups'));

        return redirect(route('categories.index'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.categories.create',[
            'categories'=>Categori::all(),
            'propGroups'=>PropertyGroup::all()
        ]);
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categori  $categori
     * @return \Illuminate\Http\Response
     */
    public function show(Categori $categori)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Categori $category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Categori $category)
    {
        $category->update([
            'title'=>$request->get('title'),
            'categori_id'=>$request->get('categori_id')
        ]);
        $category->Propgroups()->sync($request->get('propGroups'));

        return redirect (route('categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Categori $category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Categori $category)
    {
        $category->propgroups()->detach();
        $category->delete();
        return redirect(route('categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Categori $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Categori $category)
    {
        return view('admin.categories.edit',[
            'categories'=>Categori::all(),
            'cat'=> $category,
            'propGroups'=>PropertyGroup::all()
        ]);
    }
}
