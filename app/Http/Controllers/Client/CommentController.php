<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    public function store(Product $product ,Request $request)
    {
Comment::query()->create([
'product_id'=>$product->id,
    'user_id'=>auth()->id(),
    'content'=>$request->get('content')
]);

return redirect()->back();
    }
}
