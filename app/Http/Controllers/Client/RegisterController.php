<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Http\Requests\VerifyOtpRequest;
use App\Mail\otpMail;
use App\Models\Categori;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function create()
    {
        return view('client.register.create',[
            'categories'=>Categori::query()->where('categori_id',null)->get()
        ]);
    }

    public function sendmail(Request $request)
    {
        $this->validate($request,[
           'email'=>['required','email']
        ]);

       $otp= random_int (10000,99999);

       $userquery= User::query()->where('email',$request->get('email'));

       if ($userquery->exists()){
           $user=$userquery->first();
           $user->update([
              'password'=>bcrypt($otp)
           ]);
       }
       else{
           $user= User::query()->create([
               'role_id'=>Role::findbytitle('normaluser')->id ,
               'email'=>$request->get('email'),
               'password'=>bcrypt($otp)
           ]);
       }


        //send otp by email to user
     Mail::to($user->email)->send(new otpMail($otp));
        return redirect(route('client.register.otp',$user));
        //register.otp route is where user should enter otp
    }

    public function otp(User $user)
    {
        return view('client.register.otp',[
            'categories'=>Categori::query()->where('categori_id',null)->get(),
           'user'=>$user
        ]);
    }

    public function verifyOtp(User $user , VerifyOtpRequest $request)
    {
        if(!Hash::check($request->get('otp'),$user->password)){

            return back()->withErrors(['otp'=>'رمزتون اشتباهه']);
        }
        auth()->login($user);
        return redirect(route('client.index'));
    }

    public function logout()
    {
        auth()->logout();
        return redirect(route('client.index'));
    }
}
