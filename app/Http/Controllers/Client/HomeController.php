<?php

namespace App\Http\Controllers\client;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Categori;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('client.home' , [
          'categories'=>Categori::all()->where('categori_id', null)
/*            'brands'=>Brand::all()*/
            //i pass these parametes with  codes on AppServiceProvider
        ]);
    }
}
