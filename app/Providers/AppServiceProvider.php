<?php

namespace App\Providers;

use App\Models\Brand;
use App\Models\Categori;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View()->composer('client.*',function ($view){
            $view->with(
                [
/*                    'categories'=>Categori::query()->where('categori_id',null)->get(),*/
                    'brands'=>Brand::all()
                ]
            );
        });
    }
}
