<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Permission::query()->insert([
            //cate perm

                [
                'title'=>'readCat',
                'label'=>'مشاهده دسته بندی'
                ],
                [
                    'title'=>'addCat',
                    'label'=>'افزودن دسته بندی'
                ],
                [
                    'title'=>'editCat',
                    'label'=>'ویرایش دسته بندی'
                ],
                [
                    'title'=>'deleteCat',
                    'label'=>'حذف دسته بندی'
                ]
                ]);
            Permission::query()->insert([
            //product perm

                [
                    'title'=>'readPro',
                    'label'=>'مشاهده محصولات'
                ],
                [
                    'title'=>'addPro',
                    'label'=>'افزودن محصولات'
                ],
                [
                    'title'=>'editPro',
                    'label'=>'ویرایش محصولات'
                ],
                [
                    'title'=>'deletePro',
                    'label'=>'حذف محصولات'
                ]
            ]);
        Permission::query()->insert([
            //pic perm
                [
                    'title'=>'readPic',
                    'label'=>'مشاهده تصاویر'
                ],
                [
                    'title'=>'addPic',
                    'label'=>'افزودن تصاویر'
                ],
                [
                    'title'=>'editPic',
                    'label'=>'ویرایش تصاویر'
                ],
                [
                    'title'=>'deletePic',
                    'label'=>'حذف تصاویر'
                ]
            ]);


        Permission::query()->insert([

            //discount perm

                [
                    'title'=>'readDiscount',
                    'label'=>'مشاهده تخفیف'
                ],
                [
                    'title'=>'addDiscount',
                    'label'=>'افزودن تخفیف'
                ],
                [
                    'title'=>'editDiscount',
                    'label'=>'ویرایش تخفیف'
                ],
                [
                    'title'=>'deleteDiscount',
                    'label'=>'حذف تخفیف'
                ]
            ]);
        Permission::query()->insert([
            //offer perm

                [
                    'title'=>'readOffer',
                    'label'=>'مشاهده کد تخفیف'
                ],
                [
                    'title'=>'addOffer',
                    'label'=>'افزودن کد تخفیف'
                ],
                [
                    'title'=>'editOffer',
                    'label'=>'ویرایش کد تخفیف'
                ],
                [
                    'title'=>'deleteOffer',
                    'label'=>'حذف کد تخفیف'
                ]
            ]);
        Permission::query()->insert([
            //role perm

                [
                    'title'=>'addRole',
                    'label'=>'افزودن نقش'
                ],
                [
                    'title'=>'editRole',
                    'label'=>'ویرایش نقش'
                ],
                [
                    'title'=>'deleteRole',
                    'label'=>'حذف نقش'
                ]
        ]);
    }
}
