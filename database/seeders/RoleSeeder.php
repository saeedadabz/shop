<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminrole= Role::query()->create([
           'title'=>'admin'
        ]);
        $adminrole->permissions()->attach(Permission::all());

        Role::query()->create([
            'title'=>'normaluser'
        ]);
        $normaluserper=Permission::query()->whereIn('title',['reatCat'])->get();

    }
}
