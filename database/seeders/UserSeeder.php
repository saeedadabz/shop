<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $AdminUser=User::query()->create([
            'role_id'=>Role::query()->where('title','admin')->first()->id,
           'name'=>'SaeidAdabz',
            'email'=>'saeidadabzadeh20@gmail.com',
            'password'=>bcrypt(12345)
        ]);
    }
}
