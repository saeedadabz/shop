@extends('admin.layout.master')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">برند ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example5" class="table table-bordered table-striped" style="width:100%">

                            <thead>
                            <tr>
                                <td>#</td>
                                <td>اسم</td>
                                <td>تصویر</td>
                                <td>ویرایش</td>
                                <td>حذف</td>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($brands as $brand)
                                <tr>
                                    <td>{{$brand->id}}</td>
                                    <td>{{$brand->name}}</td>
                                    <td><img width="50" src="{{str_replace('public','/storage',$brand->image)}}" alt="image"></td>
                                    <td><a class="btn btn-primary" href="{{route('brands.edit', $brand->id)}}">ویرایش</a></td>
                                    <td>
                                        <form action="{{route('brands.destroy', $brand)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger" name="btn"  value="حذف">

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>اسم</th>
                                <th>دسته والد</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
@section('scripts')
    <!-- This is data table -->
    <script src="/admin/assets/vendor_components/datatable/datatables.min.js"></script>

    <!-- Superieur Admin for Data Table -->
    <script src="/admin/js/pages/data-table.js"></script>
@endsection
