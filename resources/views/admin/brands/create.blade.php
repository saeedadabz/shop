@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن برند</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('brands.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">اسم</label>
                        <input type="text" class="form-control" name="name" ><br>
                        </div>

                        <div class="form-group">
                            <label for="image"> عکس</label>
                            <input type="file"  name="image" class="form-control"><br>

                        </div>
                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

            @if(count($errors->all()) > 0 )
                @foreach($errors->all() as $error)
                    {{$error}}
                @endforeach
            @endif
            <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
