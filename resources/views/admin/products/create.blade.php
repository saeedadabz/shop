@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن محصول</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="name">اسم</label>
                            <input type="text" class="form-control" name="name" ><br>
                        </div>
                        <div class="form-group">
                            <label for="name">قیمت</label>
                            <input type="number" class="form-control" name="price" ><br>
                        </div>
                        <div class="form-group">
                            <label for="title">دسته بندی محصول</label>
                            <select name="categori_id" class="form-control">
                                <option class="" value="" disabled selected>دسته والد را وارد کنید</option><br>
                                @foreach($categories as $categori)
                                    <option value="{{$categori->id}}">{{$categori->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">برند محصول</label>
                            <select name="brand_id" class="form-control">
                                <option class="" value="" disabled selected>دسته والد را وارد کنید</option><br>
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>
                            <label for="name">اسلاگ محصول</label>
                            <input type="text" class="form-control" name="slug" ><br>
                         <div class="form-group">
                            <label for="name">توضیحات</label>
                          <textarea name="description"></textarea> <br>
                        </div>
                        <div class="form-group">
                            <label for="image"> عکس</label>
                            <input type="file"  name="image" class="form-control"><br>

                        </div>
                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
