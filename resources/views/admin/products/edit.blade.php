@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">ویرایش محصول {{$product->name }}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('products.update',$product->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <div class="form-group">
                            <label for="name">اسم</label>
                            <input type="text" value="{{$product->name}}" class="form-control" name="name" ><br>
                        </div>
                        <div class="form-group">
                            <label for="name">قیمت</label>
                            <input type="number" value="{{$product->price}}" class="form-control" name="price" ><br>
                        </div>
                        <div class="form-group">
                            <label for="title">دسته بندی محصول</label>
                            <select name="categori_id" class="form-control">
                                <option class="" value="" disabled selected>دسته والد را وارد کنید</option><br>
                                @foreach($categories as $categori)
                                    <option
                                        @if($categori->id==$product->categori_id)
                                            selected
                                            @endif
                                        value="{{$categori->id}}">{{$categori->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">برند محصول</label>
                            <select name="brand_id" class="form-control">
                                <option class="" value="" disabled selected>دسته والد را وارد کنید</option><br>
                                @foreach($brands as $brand)
                                    <option
                                        @if($brand->id==$product->brand_id)
                                       selected
                                        @endif
                                        value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="name">اسلاگ محصول</label>
                        <input type="text" value="{{$product->slug}}" class="form-control" name="slug" ><br>
                        <div class="form-group">
                            <label for="name">توضیحات</label>
                            <textarea name="description">{{$product->description}}</textarea> <br>
                        </div>
                        <div class="form-group">
                            <label for="image"> عکس</label>
                            <input type="file"  name="image" class="form-control"><br>
                            <img  src="{{str_replace('public','/storage',$product->image)}}" width="50">
                        </div>
                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
