@extends('admin.layout.master')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">برند ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example5" class="table table-bordered table-striped" style="width:100%">

                            <thead>
                            <tr>
                                <td>#</td>
                                <td>اسم</td>
                                <td>قیمت</td>
                                <td>اسلاگ</td>
                                <td>دسته بندی</td>
                                <td>برند</td>
                                <td>تصویر</td>
                                <td>تاریخ ایجاد</td>
                                <td>گالری </td>
                                <td>افزودن ویژگی </td>
                                <td>ایجاد تخفیف </td>
                                <td>ویرایش</td>
                                <td>حذف</td>

                            </tr>
                            </thead>

                            <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>{{$product->slug}}</td>
                                    <td>{{$product->categori->title}}</td>
                                    <td>{{$product->brand->name}}</td>
                                {{--    <td>{{$product->categori->title}}</td>
                                    <td>{{$product->brand->name}}</td>--}}
                                    <td><img width="50" src="{{str_replace('public','/storage',$product->image)}}" alt="image"></td>

                                    <td>{{$product->price}}</td>
                                    <td>  <a class="btn btn-warning" href="{{route('products.pictures.index', $product->id)}}">گالری</a></td>
                                    <td>  <a class="btn btn-primary" href="{{route('products.propertyProduct.index', $product->id)}}">ویژگی ها</a></td>
                                    <td>  <a class="btn btn-warning" href="{{route('products.comment.index', $product->id)}}">کامنت ها</a></td>
                                    <td>
                                    @if(!$product->discount()->exists() )
                                            <a class="btn btn-primary" href="{{route('products.discounts.create', $product)}}">ایجاد تخفیف</a>
                                    @else
                                    <p>{{$product->discount->value}}</p>
                                        <form action="{{route('products.discounts.destroy' ,['product'=>$product,'discount'=>$product->discount] )}}" method="post">
                                           @csrf
                                            @method('delete')

                                            <input class="btn btn-sm btn-danger" type="submit" name="btn" value="حذف تخفیف">

                                        </form>
                                    @endif

                                    </td>






                                    <td><a class="btn btn-primary" href="{{route('products.edit', $product->id)}}">ویرایش</a></td>
                                    <td>
                                        <form action="{{route('products.destroy', $product)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger" name="btn"  value="حذف">

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>اسم</th>
                                <th>دسته والد</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
@section('scripts')
    <!-- This is data table -->
    <script src="/admin/assets/vendor_components/datatable/datatables.min.js"></script>

    <!-- Superieur Admin for Data Table -->
    <script src="/admin/js/pages/data-table.js"></script>
@endsection
