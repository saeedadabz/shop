@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن مشخصات</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('property.store')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="title">عنوان  مشخصه</label>
                            <input type="text" class="form-control" name="title" ><br>
                        </div>
{{--// select propGroup   --}}
                        <div class="form-group">
                            <label for="title">گروه </label>
                            <select name="Property_Group_id" class="form-control">
                                <option class="" value="" disabled selected>دسته والد را وارد کنید</option><br>
                                @foreach($propertyGroups as $propGroup)
                                    <option value="{{$propGroup->id}}">{{$propGroup->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
