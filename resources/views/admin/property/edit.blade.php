@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن مشخصات</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('property.update',$property)}}" method="post">
                        @csrf
                        @method('patch')
                        <div class="form-group">
                            <label for="title">عنوان  مشخصه</label>
                            <input type="text" value="{{$property->title}}" class="form-control" name="title" ><br>
                        </div>
                        {{--// select propGroup   --}}
                        <div class="form-group">
                            <label for="title">گروه </label>
                            <select name="Property_Group_id" class="form-control">
                                <option class="" value="" disabled >دسته والد را وارد کنید</option><br>
                                @foreach($PropertyGroups as $propGroup)
                                    <option
                                        @if($property->propGroup->id ==$propGroup->id)
                                       selected
                                            @endif

                                        value="{{$propGroup->id}}">{{$propGroup->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
