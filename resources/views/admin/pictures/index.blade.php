@extends('admin.layout.master')

@section('content')
    <div class="form-group">
    <div class="row">
        <form method="post" action="{{route('products.pictures.store', $product->id)}}" enctype="multipart/form-data">
            @csrf
            <input type="file" name="image">
            <input type="submit" name="btn" value="افزودن تصویر">

        </form>
    </div>
</div>

    <div class="row">
        @foreach($product->pictures as $picture)
            <div class="col-md-12 col-lg-3">
            <div class="card">
                <img class="card-img-top img-responsive" src="{{str_replace('public','/storage', $picture->path)}}" alt="Card image cap">
                <div class="card-body">
                    <h4 class="card-title">Card title</h4>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    <form method="post" action="{{route('products.pictures.destroy',['picture'=>$picture,'product'=>$product])}}">
                       @csrf
                        @method('delete')
                        <input class="btn btn-primary" type="submit" name="btn" value="delete">
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            </div>
        @endforeach
            <div class="col-md-12 col-lg-3">
                <div class="card">
                    <img class="card-img-top img-responsive" src="{{str_replace('public','/storage', $product->image)}}" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title">Card title</h4>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <form method="post" action="{{route('products.pictures.destroy',['product'=>$product,'picture'=>$product->image])}}">
                        @csrf
                         @method('delete')
                            <input class="btn btn-primary" type="submit" name="btn" value="delete">

                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
      </div>


@endsection
