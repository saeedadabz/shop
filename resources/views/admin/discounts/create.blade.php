@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">ایجاد تخفیف</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('products.discounts.store',$product)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group" >
                            <label for="number"><b>مقدار تخفیف بر حسب درصد</b></label>
                            <input type="number" class="form-control" name="value" ><br>
                        </div>

                        <input type="submit" value="ایجاد تخفیف" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
