@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن نقش</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('roles.update',$role->id)}}" method="post">
                        @csrf
                        @method('PATCH' )
                        <div class="form-group">
                            <label for="title">عنوان</label>
                            <input type="text" value="{{$role->title}}" class="form-control" name="title"><br>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                @foreach($permissions as $permission)
                                    <label class="col-sm-2" >
                                        <input style="opacity: 1 !important; position: static !important; left: 0 ; right: 0" type="checkbox"
                                               @if($role->hasper($permission->id))
                                                   checked
                                                   @endif

                                               name="permissions[]" value="{{$permission->id}}">
                                        {{$permission->label}}
                                    </label>
                                    <br>
                                @endforeach
                            </div>
                        </div>


                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
