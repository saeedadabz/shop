@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن ویژگی به محصول:{{$product->name}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('products.propertyProduct.store',$product)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        {{-- propGroups belew  is just for choosed product--}}
                        @foreach($propGroups as $group)
                            <h3>{{$group->title}}</h3>
                            @foreach($group->properties as $prop)
                                <div class="form-group">
                                    <label for="name">{{$prop->title}}</label>
                                    <input type="text" value="{{   $prop->getvalueforproduct($product)}}" class="form-control" name="properties[{{$prop->id}}][value]" ><br>
                                </div>
                            @endforeach
                        @endforeach

                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
