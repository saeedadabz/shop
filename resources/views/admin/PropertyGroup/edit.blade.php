@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">تغییر گروه مشخصات</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('propertyGroup.update',$propertyGroup)}}" method="post">
                        @csrf
                        @method('patch')
                        <div class="form-group">
                            <label for="title">عنوان گروه مشخصات</label>
                            <input type="text" {{--value="{{$propertyGroup->title}}"--}} class="form-control" name="title" ><br>
                        </div>

                        <input type="submit" name="btn" class="btn btn-primary">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>

        @if(count($errors->all()) > 0 )
            @foreach($errors->all() as $error)
                {{$error}}
            @endforeach
        @endif
        <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
