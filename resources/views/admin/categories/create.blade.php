@extends('admin.layout.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">افزودن دسته بندی ها</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <form action="{{route('categories.store')}}" method="post">
                     @csrf
                    <div class="form-group">
                        <label for="title">دسته والد</label>
                    <select name="categori_id" class="form-control">
                        <option class="" value="" disabled selected>دسته والد را وارد کنید</option><br>
                        @foreach($categories as $categori)
                        <option value="{{$categori->id}}">{{$categori->title}}</option>
                            @endforeach
                    </select>
                    </div>
                   <div class="form-group">
                       <label for="title">عنوان</label>
                       <input type="text" class="form-control" name="title"><br>
                   </div>

                    <div class="form-group">
                        <div class="row">
                            <label>گروه ویژگی:</label><br>
                            @foreach($propGroups as $propGroup)
                                <label class="col-sm-2" >
                                    <input style="opacity: 1 !important; position: static !important; left: 0 ; right: 0" type="checkbox" name="propGroups[]" value="{{$propGroup->id}}">
                                    {{$propGroup->title}}
                                </label>
                                <br>
                            @endforeach
                        </div>
                    </div>

                    <input type="submit" name="btn" class="btn btn-primary">

                </form>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
@endsection
