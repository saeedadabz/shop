@extends('admin.layout.master')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">تغییر دسته بندی </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{route('categories.update',$cat)}}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="title">دسته والد</label>
                            <select name="categori_id" class="form-control">
                                <option class="" value="" disabled selected >دسته والد را وارد کنید</option><br>
                                @foreach($categories as $parent)
                                    <option
                                        @if($cat->categori_id == $parent->id)
                                            selected
                                        @endif
                                        value="{{$parent->id}}">{{$parent->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="title">عنوان</label>

                            <input type="text" class="form-control" name="title" value="{{$cat->title}}">
                        <br>

                        <div class="form-group">
                            <div class="row">
                                @foreach($propGroups as $propGroup)
                                    <label class="col-sm-2" >
                                        <input style="opacity: 1 !important; position: static !important; left: 0 ; right: 0" type="checkbox" name="propGroups[]"
                                               @if($cat->Propgroups()->where('Property_Group_id',$propGroup->id)->exists())
                                               checked
                                               @endif

                                               value="{{$propGroup->id}}">
                                        {{$propGroup->title}}
                                    </label>
                                    <br>
                                @endforeach
                            </div>
                        </div>

                        <input type="submit" name="btn" class="btn btn-primary" value="ثبت تغییر">

                    </form>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!-- /.col -->
    </div>
    @endsection
