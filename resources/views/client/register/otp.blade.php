@extends('client.layout.master')
@section('content')

    <<div id="container">
        <div class="container">
            <!-- Breadcrumb Start-->
            <ul class="breadcrumb">
                <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                <li><a href="login.html">حساب کاربری</a></li>
                <li><a href="register.html">ثبت نام</a></li>
            </ul>
            <!-- Breadcrumb End-->
            <div class="row">
                <!--Middle Part Start-->
                <div class="col-sm-9" id="content">
                    <h1 class="title">ثبت نام حساب کاربری</h1>
                  {{--  <p>اگر قبلا حساب کاربریتان را ایجاد کرد اید جهت ورود به <a href="login.html">صفحه لاگین</a> مراجعه کنید.</p>--}}
                    <form class="form-horizontal" method="post" action="{{route('client.register.verifyOtp',$user)}}">
                        @csrf
                        <fieldset id="account">
                            <legend>رمز یکبار مصرف ارسال شده به ایمیلتان را وارد کنید:</legend>
                            <div style="display: none;" class="form-group required">
                                <label class="col-sm-2 control-label">گروه مشتری</label>
                                <div class="col-sm-10">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" checked="checked" value="1" name="customer_group_id">
                                            پیشفرض</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label for="input-email" class="col-sm-2 control-label">رمز یکبار مصرف</label>
                                <div class="col-sm-10">
                                    <input type="text" min="5" max="5" class="form-control" id="input-email" placeholder="رمز یکبار مصرف" value="" name="otp">
                                </div>
                            </div>
                        </fieldset>

                        <div class="buttons">
                            <div class="pull-right">
                                <input type="submit" class="btn btn-primary" value="ادامه">
                            </div>
                        </div>
                    </form>
                    @if(count($errors->all())>0)
                        @foreach($errors->all() as $error)

                            {{$error}}

                        @endforeach
                    @endif
                </div>
                <!--Middle Part End -->
                <!--Right Part Start -->

                <!--Right Part End -->
            </div>
        </div>
    </div>


@endsection
